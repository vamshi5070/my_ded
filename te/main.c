#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include "./constants.h"

SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;
SDL_Surface* textSurface = NULL;
SDL_Texture* textTexture = NULL;

// flag for is game running
int game_is_running = FALSE;


#define WINDOW_WIDTH 2550
#define WINDOW_HEIGHT 1430
#define total_size 30
#define SQUARE_SIZE  ((WINDOW_WIDTH * (WINDOW_HEIGHT /2 ))/ 10000)
TTF_Font* font_large = NULL;
TTF_Font* font_small = NULL;
int visiting_index = -1;

void scc(int code){
  if (code < 0) {
    fprintf(stderr, "SDL ERROR: %s\n", SDL_GetError());
    exit(1);
  }
}

void *scp(void *ptr){
  if (ptr == NULL) {
    fprintf(stderr, "SDL ERROR: %s\n", SDL_GetError());
    exit(1);
  }
  return ptr;
}

int initialize_window(void){
  scc(SDL_Init(SDL_INIT_EVERYTHING));

  /* SDL_Window * */
  window = scp(SDL_CreateWindow("Ted", 5,5,WINDOW_WIDTH,WINDOW_HEIGHT,SDL_WINDOW_BORDERLESS));
  renderer = scp(SDL_CreateRenderer(window,-1,SDL_RENDERER_ACCELERATED));
  
  // Initialize SDL TTF
  scc(TTF_Init());

  // Load a font
  font_large = scp(TTF_OpenFont("/home/vamshi/my_ded/bitstream/VeraMono.ttf", 74));
  font_small = scp(TTF_OpenFont("/home/vamshi/my_ded/bitstream/VeraMono.ttf", 44));
  
  return TRUE;
}

int main() {
    game_is_running = initialize_window();

    while(game_is_running){
      /* process_input(); */
      /* update(); // change the values that can be affected */
      render(); // display on screen
    }

}
