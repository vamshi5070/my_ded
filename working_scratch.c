#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <stdio.h>
#include "./constants.h"
#include <stdbool.h>


// flag for is game running
int game_is_running = FALSE;
SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;
char only_str[30];
char only_str_len;

void scc(int code){
  if (code < 0) {
    fprintf(stderr, "SDL ERROR: %s\n", SDL_GetError());
    exit(1);
  }
}

void *scp(void *ptr){
  if (ptr == NULL) {
    fprintf(stderr, "SDL ERROR: %s\n", SDL_GetError());
    exit(1);
  }
  return ptr;
}

int initialize_window(void){

  scc(SDL_Init(SDL_INIT_EVERYTHING));

  /* SDL_Window * */
  window = scp(SDL_CreateWindow("Ted", 0,0,800,600,SDL_WINDOW_RESIZABLE));
  renderer = scp(SDL_CreateRenderer(window,-1,SDL_RENDERER_ACCELERATED));
  return TRUE;
}

void setup(){
  only_str_len = 0;
  /* obj.arr[ARR_SIZE]; */
  /* obj.len = 0; */
}

void process_input(){

  SDL_Event event;
  SDL_PollEvent(&event);
  
  switch(event.type) {
    
  case SDL_QUIT:
    game_is_running = FALSE;
    break;
  case SDL_KEYDOWN:
    if (event.key.keysym.sym == SDLK_ESCAPE)
      game_is_running = FALSE;
    break;
    /* break; */
  }

}

void update(){
  
}

void render(){
  scc(SDL_SetRenderDrawColor(renderer,0,0,0,255));
  scc(SDL_RenderClear(renderer));
  
  SDL_RenderPresent(renderer); 
}

void destroy_window(){
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();
}

int main() {

  game_is_running = initialize_window();

  setup();

  while(game_is_running){
    process_input();
    update(); // change the values that can be affected
    render(); // display on screen
  }
  destroy_window();

  return 0;
}
