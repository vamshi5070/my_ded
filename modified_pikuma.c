#include <stdio.h>
#include <SDL2/SDL.h>
#include "./constants.h"
#include <SDL2/SDL_ttf.h>

int game_is_running = FALSE;
SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;
int last_frame_time = 0;

struct ball {
  float x;
  float y;
  float width;
  float height;
} ball;

int initialize_window(void){
  if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
    fprintf(stderr, "Error initializing SDL.\n");
    return FALSE;
  }

  window = SDL_CreateWindow(
			    NULL,
			    SDL_WINDOWPOS_CENTERED,
			    SDL_WINDOWPOS_CENTERED,
			    WINDOW_WIDTH,
			    WINDOW_HEIGHT,
			    SDL_WINDOW_BORDERLESS);

  
  if (!window) {
    fprintf(stderr,"Error creating SDL Window.\n");
    return FALSE;
  }

  renderer = SDL_CreateRenderer(window, -1, 0);

  if (!renderer) {
    fprintf(stderr,"Error creating SDL Renderer.\n");
    return FALSE;
  }
  
  return TRUE;
}


void process_input() {
  SDL_Event event;
  SDL_PollEvent(&event);

  switch (event.type) {
  case SDL_QUIT:
    game_is_running = FALSE;
    break;
  case SDL_KEYDOWN:
    if (event.key.keysym.sym == SDLK_ESCAPE)
      game_is_running = FALSE;
    break;
  }
}

void setup() {
  ball.x = 20;
  ball.y = 20;
  ball.width = 15;
  ball.height = 15;
  // TODO:
}

void update() {

  /* while(!SDL_TICKS_PASSED(SDL_GetTicks(), last_frame_time + FRAME_TARGET_TIME)); */

  int time_to_wait = FRAME_TARGET_TIME - (SDL_GetTicks() - last_frame_time);

  if (time_to_wait > 0 && time_to_wait <= FRAME_TARGET_TIME) {
    SDL_Delay(time_to_wait);
  }
  
  float delta_time = (SDL_GetTicks() - last_frame_time) / 1000.0f;
  
  last_frame_time = SDL_GetTicks();
  
  ball.x = ball.x + 70 * delta_time;
  ball.y = ball.y + 50 * delta_time;
  // TODO:
}

void render() {
  SDL_SetRenderDrawColor(renderer,0,0,0,255);
  SDL_RenderClear(renderer);

  SDL_Rect ball_rect = {
    (int)ball.x
    ,(int)ball.y
    ,(int)ball.width
    ,(int)ball.height
  };

  /* SDL_SetRenderDrawColor(renderer,255,255,255,255); */
  /* SDL_RenderFillRect(renderer, &ball_rect); */
  
  SDL_RenderPresent(renderer); 
}

void destroy_window(){
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();
}

int main(){
  
  // Initialize SDL_ttf
  if (TTF_Init() != 0) {
    SDL_Log("Unable to initialize SDL_ttf: %s", TTF_GetError());
    SDL_Quit();
    return 1;
  }


  // Load a TrueType font
  TTF_Font* font = TTF_OpenFont("./bitstream/VeraMono.ttf", 24);
  if (!font) {
    SDL_Log("Unable to load font: %s", TTF_GetError());
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    TTF_Quit();
    SDL_Quit();
    return 1;
  }


  // Set the text color (in RGB format)
  SDL_Color textColor = {0, 0, 0};
  
  // Create a text surface
  SDL_Surface* textSurface = TTF_RenderText_Solid(font, "[Hello, SDL2!]", textColor);
  if (!textSurface) {
    SDL_Log("Unable to render text: %s", TTF_GetError());
    TTF_CloseFont(font);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    TTF_Quit();
    SDL_Quit();
    return 1;
  }
  
  // Create a texture from the surface
  SDL_Texture* textTexture = SDL_CreateTextureFromSurface(renderer, textSurface);
  if (!textTexture) {
    SDL_Log("Unable to create texture: %s", SDL_GetError());
    SDL_FreeSurface(textSurface);
    TTF_CloseFont(font);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    TTF_Quit();
    SDL_Quit();
    return 1;
  }
  
  // Query the dimensions of the texture
  SDL_Rect textRect;
  textRect.x = 100;
  textRect.y = 100;
  textRect.w = textSurface->w;
  textRect.h = textSurface->h;
  
  
  game_is_running = initialize_window();

  setup();
  
  while(game_is_running) {
    process_input();
    update();
    render();
  }
  destroy_window();
  return 0;
}
