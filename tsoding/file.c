/* #ifndef FILE_H_ */
/* #define FILE_H_ */
#include "./file.h"

char *slurp_file(const char *file_path, size_t *size){
  /* const char *file_path_cstr = arena_sv_to_cstr(arena,file_path); */

  FILE *f = fopen(file_path_cstr, "rb");
  if (f == NULL){
    return -1;
  }

  if (fseek(f,0,SEEK_END) < 0){
    return -1;
  }

  long m = ftell(f);
  if (m < 0){
    return -1;
  }

  char *buffer = arena_alloc(arena,(size_t) m);
  if (buffer == NULL){
    return -1;
  }

  if (fseek(f, 0, SEEK_SET) < 0){
    return -1;
  }

  size_t n = fread(buffer,1,(size_t) m, f);
  if (ferror(f)){
    return -1;
  }

  fclose(f);
  
  if (content){
    content->count = n;
    content->data = buffer;
  }
  return 0;
}


