#ifndef SDL_EXTRA_H_
#define SDL_EXTRA_H_

#include <SDL2/SDL.h>

#include "./la.h"

#define UNHEX(color) \
  ((color) >> (8 * 0)) & 0xFF,\
  ((color) >> (8 * 1)) & 0xFF,\
  ((color) >> (8 * 2)) & 0xFF,\
  ((color) >> (8 * 3)) & 0xFF

void scc(int code);
void scp(void* ptr);

SDL_Surface *surface_from_file(const char *file_path);
Vec2f window_size(SDL_Window *window);

#endif
