#ifndef FILE_H_
#define FILE_H_

char *slurp_file(const char *file_path, size_t *size);

#endif // FILE_H_
