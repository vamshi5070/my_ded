#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>

// Constants for screen dimensions
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
/* const int SCREEN_WIDTH = 800; */
/* const int SCREEN_HEIGHT = 600; */
const int SQUARE_SIZE = 50;
TTF_Font* gFont = NULL;
SDL_Window* gWindow = nullptr;
SDL_Renderer* gRenderer = nullptr;

// Function to render text on the screen
void renderText(SDL_Renderer* renderer, TTF_Font* font, const char* text, int x, int y) {
    SDL_Surface* textSurface = TTF_RenderText_Solid(font, text, (SDL_Color){255, 255, 255, 255});
    SDL_Texture* textTexture = SDL_CreateTextureFromSurface(renderer, textSurface);

    SDL_Rect textRect;
    textRect.x = x;
    textRect.y = y;
    textRect.w = textSurface->w;
    textRect.h = textSurface->h;

    SDL_RenderCopy(renderer, textTexture, NULL, &textRect);

    SDL_FreeSurface(textSurface);
    SDL_DestroyTexture(textTexture);
}

void drawSquareWithNumber(int x, int y, int number) {
  SDL_Rect squareRect = { x, y, SQUARE_SIZE, SQUARE_SIZE };
  SDL_SetRenderDrawColor(gRenderer, 0, 0, 0, 255); // Square color (black)
  SDL_RenderFillRect(gRenderer, &squareRect);
  SDL_Color textColor = { 255, 255, 255 }; // Text color (white)
  
  char numberText[10];
  snprintf(numberText, sizeof(numberText), "%d", number);
  
  SDL_Surface* textSurface = TTF_RenderText_Solid(gFont, numberText, textColor);
  SDL_Texture* textTexture = SDL_CreateTextureFromSurface(gRenderer, textSurface);
  SDL_Rect textRect = { x + SQUARE_SIZE / 2 - textSurface->w / 2, y + SQUARE_SIZE / 2 - textSurface->h / 2, textSurface->w, textSurface->h };
  
  SDL_RenderCopy(gRenderer, textTexture, NULL, &textRect);
  SDL_DestroyTexture(textTexture);
  SDL_FreeSurface(textSurface);
}

int main(int argc, char* args[]) {
    SDL_Window* window = NULL;
    SDL_Renderer* renderer = NULL;
    TTF_Font* font = NULL;

    // Initialize SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
        return 1;
    }

    // Create a window
    window = SDL_CreateWindow("Name Display", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    if (window == NULL) {
        printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
        return 1;
    }

    // Create a renderer
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == NULL) {
        printf("Renderer could not be created! SDL_Error: %s\n", SDL_GetError());
        return 1;
    }

    // Initialize SDL TTF
    if (TTF_Init() == -1) {
        printf("SDL TTF could not initialize! SDL_Error: %s\n", TTF_GetError());
        return 1;
    }

    // Load a font
    font = TTF_OpenFont("./bitstream/VeraMono.ttf", 28);
    if (font == NULL) {
        printf("Font could not be loaded! TTF_Error: %s\n", TTF_GetError());
        return 1;
    }

    // Main loop
    int quit = 0;
    SDL_Event e;
    char name[256] = "Enter your name: ";
    char temp[15] = "";
    char command[15] = "r";
    int nameLength = strlen(name);

    /* int my_arr[6] = {1,2,3,4,5}; */
    /* int my_arr_length = 5; */

    while (!quit) {
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                quit = 1;
            } else if (e.type == SDL_TEXTINPUT) {
                if (nameLength < 255) {
                    strcat(name, e.text.text);
		    strcat(temp,e.text.text);
                    nameLength++;
                }
            } else if (e.type == SDL_KEYDOWN) {
                if (e.key.keysym.sym == SDLK_RETURN) {

		  strcat(command,temp);

		  temp[0] = '\0';
		  printf("You entered: %s\n", name);
		  printf("New name: %s\n", command);
                }
            }
        }

        // Clear the screen
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);

        // Render the text
        renderText(renderer, font, name, SCREEN_WIDTH/8, SCREEN_HEIGHT*3/4);

	drawSquareWithNumber(SCREEN_WIDTH/2, SCREEN_HEIGHT*1/2, 0 + 1);

        // Update the screen
        /* SDL_RenderPresent(renderer); */

	/* renderText(renderer, font, command, SCREEN_WIDTH/8, SCREEN_HEIGHT*1/2); */
	// Update the screen
        SDL_RenderPresent(renderer);

    }

    // Cleanup and exit
    TTF_CloseFont(font);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    TTF_Quit();
    SDL_Quit();

    return 0;
}
