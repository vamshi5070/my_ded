/*let
  pkgs = import <nixpkgs> {};
in
pkgs.stdenv.mkDerivation {
  name = "my-dervation";
  src = ./.;
  installPhase = ''
             echo Hello > $out
             '';
}*/
{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    
  };
  outputs = {self,nixpkgs}: {
    defaultPackage.aarch64-linux =
      with import nixpkgs {system = "aarch64-linux";};
      stdenv.mkDerivation {
        name = "hello";
        src = self;
        # buildPhase = "ls";
        buildInputs = [pkgs.gcc];
        buildPhase = "gcc -o hello ./hello.c";
        installPhase = "mkdir -p $out/bin; install -t $out/bin hello";
      };
  };
}
