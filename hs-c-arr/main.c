#include <stdio.h>
#include "HsFFI.h"

extern int* addElem(char*,int*);

int main() {
  hs_init(NULL,NULL);
  char number[20];
  printf("Enter any command: ");
  int arr[] = {1,2,3,4,5};
  
  if(!scanf("%s",number)){
    printf("Failed to get anything. ");
    return -1;
  }

  printf("Entered %s\n",number);
  int *modifiedArr = addElem(number,arr); 
  /* printf("Upcased string %s",modifiedString); */
  for (int i = 0; i < 6; i++) {
    printf("%d ", modifiedArr[i]);
  }
  return 0;
  hs_exit();
}
