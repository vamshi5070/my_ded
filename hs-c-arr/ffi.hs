{-# LANGUAGE ForeignFunctionInterface #-}

import Foreign.C.Types
import Foreign.C.String
import Foreign.Ptr
import Foreign.Marshal.Array
import Data.Char

foreign export ccall addElem :: CString -> Ptr CInt ->  IO (Ptr CInt)

addElem :: CString -> Ptr CInt ->  IO (Ptr CInt)
addElem str arrPtr = do
  strVal <- peekCString str
  let num = read strVal :: CInt
  arr <- peekArray 5 arrPtr
  let newArr = num:arr
  retArr <- newArray newArr
  return retArr
  -- newCString modified

main = return ()
