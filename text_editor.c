#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <stdio.h>
#include "./constants.h"
#include <stdbool.h>

#define WINDOW_WIDTH 2550
#define WINDOW_HEIGHT 1430

// flag for is game running
int game_is_running = FALSE;
SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;
SDL_Surface* textSurface = NULL;
SDL_Texture* textTexture = NULL;

TTF_Font* font_small = NULL;
TTF_Font* font_large = NULL;
char name[20] = "can't be empty";
char text_input[30] = "";
size_t text_input_size = 0;
const ARR_SIZE = 10; 
struct List_Object {
  int arr[10];
  int len;
} obj;
  
struct Pos {
  int x;
  int y;
};

struct Pos pos =  {1,1};

void scc(int code){
  if (code < 0) {
    fprintf(stderr, "SDL ERROR: %s\n", SDL_GetError());
    exit(1);
  }
}

void *scp(void *ptr){
  if (ptr == NULL) {
    fprintf(stderr, "SDL ERROR: %s\n", SDL_GetError());
    exit(1);
  }
  return ptr;
}

// Function to render text on the screen
void renderText(const char* text, TTF_Font* font, int x, int y) {
  if (strlen(text) == 0) {
    return;
  }
  
  textSurface = TTF_RenderText_Solid(font, text, (SDL_Color){255, 255, 255, 255});
  textTexture = SDL_CreateTextureFromSurface(renderer, textSurface);
  
  SDL_Rect textRect;
  textRect.x = x;
  textRect.y = y;
  textRect.w = textSurface->w;
  textRect.h = textSurface->h;
  
  SDL_RenderCopy(renderer, textTexture, NULL, &textRect);

}

int initialize_window(void){
  scc(SDL_Init(SDL_INIT_EVERYTHING));

  /* SDL_Window * */
  window = scp(SDL_CreateWindow("Ted", 5,5,WINDOW_WIDTH,WINDOW_HEIGHT,SDL_WINDOW_BORDERLESS));
  renderer = scp(SDL_CreateRenderer(window,-1,SDL_RENDERER_ACCELERATED));
  
  // Initialize SDL TTF
  scc(TTF_Init());
    /* printf("SDL TTF could not initialize! SDL_Error: %s\n", TTF_GetError()); */
    /* return 1; */
  /* } */
  
  // Load a font
  font_large = scp(TTF_OpenFont("/home/vamshi/my_ded/bitstream/VeraMono.ttf", 74));
  font_small = scp(TTF_OpenFont("/home/vamshi/my_ded/bitstream/VeraMono.ttf", 44));
  
  return TRUE;
}

void setup(){
  /* obj.arr[ARR_SIZE]; */
  obj.len = 0;
}

void process_input(){
  SDL_Event event;
  
  bool ctrlPressed = false;
  bool xPressed = false;
  bool cPressed = false;
  bool takeInput = false;

  while (SDL_PollEvent(&event)){
    switch(event.type) {
      
    case SDL_QUIT:
      game_is_running = FALSE;
      break;
    case SDL_KEYDOWN:
      if (event.key.keysym.sym == SDLK_BACKSPACE) {
	if (text_input_size < 1) {
	  return;
	}
	printf("buffer size: %d",text_input_size);
	text_input_size = text_input_size - 1;
	/* printf("text input: %d\n",text_input_size-1); */
	text_input[text_input_size] = '\0';
      }
      if (event.key.keysym.sym == SDLK_LCTRL || event.key.keysym.sym == SDLK_RCTRL){
	/* printf("Ctrl was pressed\n"); */
	ctrlPressed = true;
      }
      if (ctrlPressed && event.key.keysym.sym == SDLK_x) {
	xPressed = true;
	printf("C-x was pressed\n");
      }
      if (ctrlPressed && event.key.keysym.sym == SDLK_a) {
	takeInput = true;
      }
      /* if (takeInput && event.key.keysym.sym >= SDLK_0 && event.key.keysym.sym <=  SDLK_9) { */
      /* if  */
      /* } */
      /* if (ctrlPressed && event.key.keysym.sym == SDLK_C) { */
      /* cPressed = true; */
      /* } */
      if (event.type == SDL_KEYUP) {
	if (event.key.keysym.sym == SDLK_LCTRL || event.key.keysym.sym == SDLK_RCTRL){
	  ctrlPressed = false;
	}
      }
      break;
    case SDL_TEXTINPUT:
      if (text_input_size >= sizeof(text_input)) {
	return;
      }

      const size_t char_size = strlen(event.text.text);
      /* strlen(text_input); */
      
      text_input_size = text_input_size + char_size;
	/* sizeof(text_input) - text_input_size; */
      
	/* strlen(text_input); */
	
      printf("buffer size: %d\n",sizeof(text_input) - text_input_size - char_size);
	
      /* const size_t free_space =  */
      strncat(text_input, event.text.text ,sizeof(text_input) - text_input_size - char_size);
	      /* buffer_size - char_size); */
	      /* sizeof(text_input) - strlen(text_input) - char_size); */
      break;
    }
  }

  if (xPressed == true) {
    game_is_running  = FALSE;
  }

  /* xPressed = false; */
  /* cPressed = false; */
}

void update(){
  
}

void render(){
  SDL_SetRenderDrawColor(renderer,0,0,0,255);
  SDL_RenderClear(renderer);

  renderText(text_input,font_small, 1, 1);
  
  SDL_Color line_color = {255,255,255,255};
  drawCursor(1,1,line_color);
  /* drawHorizontalLine(0,WINDOW_HEIGHT*3/4,WINDOW_WIDTH,line_color); */
  /* const char prompt[20] = "Enter anything:   "; */
  /* renderText(prompt,font_small,WINDOW_WIDTH/8, (WINDOW_HEIGHT*3/4 + 5)); */
  /* drawCursor(text_input_size-1, */
  /* drawBorder(renderer,5,line_color); */
  SDL_FreeSurface(textSurface);
  SDL_DestroyTexture(textTexture);
  SDL_RenderPresent(renderer); 
}

void destroy_window(){
  TTF_CloseFont(font_large);
  TTF_CloseFont(font_small);

  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  TTF_Quit();
  SDL_Quit();
}

void drawHorizontalLine(int x1,int y,int x2,SDL_Color color){
  SDL_SetRenderDrawColor(renderer,color.r,color.g,color.b,color.a);
  SDL_RenderDrawLine(renderer,x1,y,x2,y);
}

void drawCursor(int x,int y,SDL_Color color){
  SDL_SetRenderDrawColor(renderer,color.r,color.g,color.b,color.a);
  /* SDL_RenderDrawLine(renderer,x1,y,x2,y); */
  
  SDL_Rect cursor = {x+0,y,x+1,50};
  SDL_RenderFillRect(renderer,&cursor);
}


void drawBorder(int thickness,SDL_Color color){
  SDL_SetRenderDrawColor(renderer,color.r,color.g,color.b,color.a);
  
  SDL_Rect topBorder = {0,0,WINDOW_WIDTH,thickness};
  SDL_RenderFillRect(renderer,&topBorder);

  /* SDL_Rect bottomBorder = {0,WINDOW_HEIGHT-thickness,WINDOW_WIDTH,WINDOW_HEIGHT}; */
  /* SDL_RenderFillRect(renderer,&bottomBorder); */
  /* SDL_Rect leftBorder = {0,0,thickness,WINDOW_HEIGHT}; */
  /* SDL_RenderFillRect(renderer,&leftBorder); */
  /* SDL_Rect rightBorder = {WINDOW_WIDTH - thickness,0,thickness,WINDOW_WIDTH}; */
  /* SDL_RenderFillRect(renderer,&rightBorder); */

}

int main() {
  /* printf("%s",name); */

  game_is_running = initialize_window();

  setup();

  while(game_is_running){
    process_input();
    printf("my buffer size:  %ld\n", text_input_size);
    update(); // change the values that can be affected
    render(); // display on screen
  }
  destroy_window();
  /* printf("Todo: Implement text input"); */
  return 0;
}
