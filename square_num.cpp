#include <iostream>
#include <SDL.h>
// #include <SDL.h>
#include <SDL2/SDL_ttf.h>

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;
const int NUM_SQUARES = 5;
const int SQUARE_SIZE = 50;

SDL_Window* gWindow = nullptr;
SDL_Renderer* gRenderer = nullptr;
TTF_Font* gFont = nullptr;

bool init() {
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        std::cerr << "SDL could not initialize! SDL Error: " << SDL_GetError() << std::endl;
        return false;
    }

    gWindow = SDL_CreateWindow("Squares with Numbers",
			       SDL_WINDOWPOS_CENTERED,
			       SDL_WINDOWPOS_CENTERED,
			       // SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
			       SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    if (gWindow == nullptr) {
        std::cerr << "Window could not be created! SDL Error: " << SDL_GetError() << std::endl;
        return false;
    }

    gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED);
    if (gRenderer == nullptr) {
        std::cerr << "Renderer could not be created! SDL Error: " << SDL_GetError() << std::endl;
        return false;
    }

    if (TTF_Init() == -1) {
        std::cerr << "SDL_ttf could not initialize! TTF Error: " << TTF_GetError() << std::endl;
        return false;
    }

    return true;
}

bool loadMedia() {
  
    gFont = TTF_OpenFont("./bitstream/VeraMono.ttf", 24);
    if (gFont == nullptr) {
        std::cerr << "Failed to load font! TTF Error: " << TTF_GetError() << std::endl;
        return false;
    }

    return true;
}

void close() {
    TTF_CloseFont(gFont);
    gFont = nullptr;
    SDL_DestroyRenderer(gRenderer);
    SDL_DestroyWindow(gWindow);
    gWindow = nullptr;
    TTF_Quit();
    SDL_Quit();
}

void drawSquareWithNumber(int x, int y, int number) {
    SDL_Rect squareRect = { x, y, SQUARE_SIZE, SQUARE_SIZE };
    SDL_SetRenderDrawColor(gRenderer, 0, 0, 0, 255); // Square color (black)
    SDL_RenderFillRect(gRenderer, &squareRect);

    SDL_Color textColor = { 255, 255, 255 }; // Text color (white)

    char numberText[10];
    snprintf(numberText, sizeof(numberText), "%d", number);

    SDL_Surface* textSurface = TTF_RenderText_Solid(gFont, numberText, textColor);
    SDL_Texture* textTexture = SDL_CreateTextureFromSurface(gRenderer, textSurface);
    SDL_Rect textRect = { x + SQUARE_SIZE / 2 - textSurface->w / 2, y + SQUARE_SIZE / 2 - textSurface->h / 2, textSurface->w, textSurface->h };

    SDL_RenderCopy(gRenderer, textTexture, nullptr, &textRect);
    SDL_DestroyTexture(textTexture);
    SDL_FreeSurface(textSurface);
}

int main(int argc, char* args[]) {
    if (!init() || !loadMedia()) {
        std::cerr << "Initialization failed!" << std::endl;
        return 1;
    }

    SDL_Event e;
    bool quit = false;

    while (!quit) {
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                quit = true;
            }
        }

        SDL_SetRenderDrawColor(gRenderer, 255, 255, 255, 255); // Background color (white)
        SDL_RenderClear(gRenderer);

        for (int i = 0; i < NUM_SQUARES; i++) {
            int x = i * SQUARE_SIZE + 1;
            int y = SCREEN_HEIGHT / 2 - SQUARE_SIZE / 2;
            drawSquareWithNumber(x, y, i + 1);
        }

        SDL_RenderPresent(gRenderer);
    }

    close();
    return 0;
}
