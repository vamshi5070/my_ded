#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <stdio.h>
#include "./constants.h"
#include <stdbool.h>

// flag for is game running
int game_is_running = FALSE;
SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;
const ARR_SIZE = 10; 
struct List_Object {
  int arr[10];
  int len;
} obj;
  
void scc(int code){
  if (code < 0) {
    fprintf(stderr, "SDL ERROR: %s\n", SDL_GetError());
    exit(1);
  }
}

void *scp(void *ptr){
  if (ptr == NULL) {
    fprintf(stderr, "SDL ERROR: %s\n", SDL_GetError());
    exit(1);
  }
  return ptr;
}

int initialize_window(void){
  scc(SDL_Init(SDL_INIT_EVERYTHING));

  /* SDL_Window * */
  window = scp(SDL_CreateWindow("Ted", 0,0,800,600,SDL_WINDOW_RESIZABLE));
  renderer = scp(SDL_CreateRenderer(window,-1,SDL_RENDERER_ACCELERATED));

  return TRUE;
}

void setup(){
  /* obj.arr[ARR_SIZE]; */
  obj.len = 0;
}

void process_input(){
  SDL_Event event;
  SDL_PollEvent(&event);
  bool ctrlPressed = false;
  bool xPressed = false;
  bool cPressed = false;
  bool takeInput = false;
  
  switch(event.type) {
    
  case SDL_QUIT:
    game_is_running = FALSE;
    break;
  case SDL_KEYDOWN:
    if (event.key.keysym.sym == SDLK_LCTRL || event.key.keysym.sym == SDLK_RCTRL){
      ctrlPressed = true;
    }
    if (ctrlPressed && event.key.keysym.sym == SDLK_x) {
      xPressed = true;
    }
    if (ctrlPressed && event.key.keysym.sym == SDLK_a) {
      takeInput = true;
    }
    /* if (takeInput && event.key.keysym.sym >= SDLK_0 && event.key.keysym.sym <=  SDLK_9) { */
      /* if  */
    /* } */
    /* if (ctrlPressed && event.key.keysym.sym == SDLK_C) { */
      /* cPressed = true; */
    /* } */
    if (event.type == SDL_KEYUP) {
      if (event.key.keysym.sym == SDLK_LCTRL || event.key.keysym.sym == SDLK_RCTRL){
	ctrlPressed = false;
      }
    }
    break;
  }

  if (xPressed) {
    game_is_running  = FALSE;
  }

  /* xPressed = false; */
  /* cPressed = false; */
}

void update(){
  
}

void render(){
  SDL_SetRenderDrawColor(renderer,0,0,0,255);
  SDL_RenderClear(renderer);
  SDL_RenderPresent(renderer); 
}

void destroy_window(){
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();
}

int main() {

  game_is_running = initialize_window();

  setup();

  while(game_is_running){
    process_input();
    update(); // change the values that can be affected
    render(); // display on screen
  }
  destroy_window();
  /* printf("Todo: Implement text input"); */
  return 0;
}
