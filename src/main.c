#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <string.h>
#include "./constants.h"
#include <stdbool.h>

#include "./render.c"
/* #include "./my_globals.c" */

bool ctrlPressed = false;
bool xPressed = false;

// flag for is game running
int game_is_running = FALSE;

/* SDL_Renderer* renderer = NULL; */
/* SDL_Surface* textSurface = NULL; */
/* SDL_Texture* textTexture = NULL; */
char msg_in_buffer[30] = "";
/* int only_number = 0; */


char name[20] = "can't be empty";
int startTime = 0;
/* = SDL_GetTicks(); */
int displayTime = 10000;

/* int no_of_enter = 0; */

char text_input[total_size] = "";
size_t text_input_size = 0;
/* const ARR_SIZE = 10;  */
int arr[20];
int arr_len = 0;
  
struct Pos {
  int x;
  int y;
};

struct Pos pos =  {1,1};

void scc(int code){
  if (code < 0) {
    fprintf(stderr, "SDL ERROR: %s\n", SDL_GetError());
    exit(1);
  }
}

void *scp(void *ptr){
  if (ptr == NULL) {
    fprintf(stderr, "SDL ERROR: %s\n", SDL_GetError());
    exit(1);
  }
  return ptr;
}

int initialize_window(void){
  scc(SDL_Init(SDL_INIT_EVERYTHING));

  /* SDL_Window * */
  window = scp(SDL_CreateWindow("Ted", 5,5,WINDOW_WIDTH,WINDOW_HEIGHT,SDL_WINDOW_BORDERLESS));
  renderer = scp(SDL_CreateRenderer(window,-1,SDL_RENDERER_ACCELERATED));
  
  // Initialize SDL TTF
  scc(TTF_Init());

  // Load a font
  font_large = scp(TTF_OpenFont("/home/vamshi/my_ded/bitstream/VeraMono.ttf", 74));
  font_small = scp(TTF_OpenFont("/home/vamshi/my_ded/bitstream/VeraMono.ttf", 44));
  
  return TRUE;
}

void setup(){
  /* obj.arr[ARR_SIZE]; */
  /* obj.len = 0; */
}

void process_input(){
  SDL_Event event;
  
  bool cPressed = false;
  bool takeInput = false;

  while (SDL_PollEvent(&event)){
    switch(event.type) {
      
    case SDL_QUIT:
      game_is_running = FALSE;
      break;
    case SDL_KEYUP:
      if (event.key.keysym.sym == SDLK_LCTRL || event.key.keysym.sym == SDLK_RCTRL){
	printf("C was released\n");
	ctrlPressed = false;
      }
      break;
    case SDL_KEYDOWN:
      if (ctrlPressed == true){
	printf("Ctrl was pressed\n");
      }

      if (event.key.keysym.sym == SDLK_BACKSPACE) {
	if (text_input_size < 1) {
	  return;
	}
	/* printf("buffer size: %d",text_input_size); */
	text_input_size = text_input_size - 1;
	/* printf("text input: %d\n",text_input_size-1); */
	text_input[text_input_size] = '\0';
      }
      if (event.key.keysym.sym == SDLK_RETURN || event.key.keysym.sym == SDLK_KP_ENTER) {

	char *strings[20];
	int strings_len = 0;

	if (text_input[strlen(text_input) - 1] == '\n') {
	  text_input[strlen(text_input) - 1] == '\0';
	}

	char *token;
	token = strtok(text_input, " "); 
	while (token != NULL) {
	  if (strings_len >= 20) {
	    printf("Array is full");
	    break;
	  }
	  
	  strings[strings_len] = strdup(token);
	  token = strtok(NULL, " ");
	  
	  strings_len = strings_len + 1;
	}

	if((strcmp(strings[0],"exit") == 0) || (strcmp(strings[0],"exit\n") == 0)) {
	  game_is_running = false;
	  return;
	  /* break; */
	} else {
	  /* process_command(strings[0],strings[1]);     */
	  
	  if (strcmp(strings[0],"add") == 0) {
	      if (arr_len >= 20) {
		strcpy(msg_in_buffer,"close the applicatoin");
		text_input[0] = '\0';
		text_input_size = 0;
		return;
	      }
	      char *endptr;
	      long num = strtol(strings[1],&endptr,10);
	      if (*endptr != '\0'){
		strcpy(msg_in_buffer,text_input);
		text_input[0] = '\0';
		text_input_size = 0;
		return;
	      } else {
		arr[arr_len] = num;
		/* atoi(text_input); */
		char temp[15]; 
		snprintf(temp,sizeof(temp),"Added %d",arr[arr_len]);
		arr_len = arr_len + 1;
		strcpy(msg_in_buffer,temp);
		text_input[0] = '\0';
		text_input_size = 0;
		return;
	      }
	    }
	  else if(strcmp(strings[0],"visit") == 0) {
	    char *endptr;
	    long num = strtol(strings[1],&endptr,10);
	    if (*endptr != '\0'){
	      strcpy(msg_in_buffer,text_input);
	      text_input[0] = '\0';
	      text_input_size = 0;
	      return;
	    } else {
	      /* arr[arr_len] = num; */
	      /* atoi(text_input); */
	      visiting_index = num;
	      char temp[15]; 
	      snprintf(temp,sizeof(temp),"visiting %d",num);
	      /* arr_len = arr_len + 1; */
	      strcpy(msg_in_buffer,temp);
	      text_input[0] = '\0';
	      text_input_size = 0;
	      return;
	    }
	  }
	}
	text_input[0] = '\0';
	text_input_size = 0;
      }
      if (event.key.keysym.sym == SDLK_LCTRL || event.key.keysym.sym == SDLK_RCTRL){
	/* printf("Ctrl was pressed\n"); */
	ctrlPressed = true;
      }
      if (ctrlPressed && event.key.keysym.sym == SDLK_x) {
	xPressed = true;
	printf("C-x was pressed\n");
      }
      if (ctrlPressed && event.key.keysym.sym == SDLK_a) {
	takeInput = true;
      }
      break;
    case SDL_TEXTINPUT:
      if (text_input_size >= sizeof(text_input)) {
	return;
      }
      const size_t char_size = strlen(event.text.text);
      /* strlen(text_input); */
      
      text_input_size = text_input_size + char_size;

      strncat(text_input, event.text.text ,sizeof(text_input) - text_input_size - char_size);
      break;
    }
  }

}

void update(){
  /* dsdsdsdsds */
}

void render(){
  const char prompt[20] = "Enter anything:";
  int textWidth, textHeight;
  SDL_Color line_color = {255,255,255,255};
  SDL_Color blue_color = {100,155,255,255};

  SDL_SetRenderDrawColor(renderer,0,0,0,255);
  SDL_RenderClear(renderer);
  for(int i = 0; i < arr_len; i++){
    drawSquareWithNumber(WINDOW_WIDTH/16 + (SQUARE_SIZE/2 * i) + ((SQUARE_SIZE) * i),WINDOW_HEIGHT/2 - SQUARE_SIZE,arr[i],i);
  }
  TTF_SizeText(font_small,prompt,&textWidth,&textHeight);  
  renderText(text_input,font_small,WINDOW_WIDTH/8 + textWidth  , (WINDOW_HEIGHT*3/4 + 5),blue_color);

  drawHorizontalLine(0,WINDOW_HEIGHT*3/4,WINDOW_WIDTH,line_color);
  
  renderText(prompt,font_small,WINDOW_WIDTH/8, (WINDOW_HEIGHT*3/4 + 5),line_color);

  drawMsgBuffer(blue_color);
  SDL_Color textColor = { 200, 200, 0};
  int y = WINDOW_HEIGHT * 7/8;
  renderText(msg_in_buffer,font_small,WINDOW_WIDTH / 32, y + y/50,textColor);
  
  SDL_FreeSurface(textSurface);
  SDL_DestroyTexture(textTexture);
  SDL_RenderPresent(renderer); 
}

void destroy_window(){
  TTF_CloseFont(font_large);
  TTF_CloseFont(font_small);

  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  TTF_Quit();
  SDL_Quit();
}

void drawHorizontalLine(int x1,int y,int x2,SDL_Color color){
  SDL_SetRenderDrawColor(renderer,color.r,color.g,color.b,color.a);
  SDL_RenderDrawLine(renderer,x1,y,x2,y);
}

void drawCursor(int x,int y,SDL_Color color){
  SDL_SetRenderDrawColor(renderer,color.r,color.g,color.b,color.a);

  SDL_Rect cursor = {x+0,y,x+1,50};
  SDL_RenderFillRect(renderer,&cursor);
}


void drawBorder(int thickness,SDL_Color color){
  SDL_SetRenderDrawColor(renderer,color.r,color.g,color.b,color.a);
  
  SDL_Rect topBorder = {0,0,WINDOW_WIDTH,thickness};
  SDL_RenderFillRect(renderer,&topBorder);

}

int main() {
  /* printf("%d",SQUARE_SIZE); */
  startTime = SDL_GetTicks();
  displayTime = 10000;
  char temp[10] = "Welcome";
  if (strlen(msg_in_buffer) == 0){
    strncat(msg_in_buffer, temp, strlen(temp));
  }
  
  game_is_running = initialize_window();

  setup();

  while(game_is_running){
    process_input();
    update(); // change the values that can be affected
    render(); // display on screen
  }
  
  destroy_window();
  if (ctrlPressed == true){
    printf("C-x pressed in final");
  }
  for (int i = 0; i < arr_len; i++) {
    printf("%d  ",arr[i]);
  }
  printf("\n");
  /* printf("No of enter: %d \n",no_of_enter); */
  return 0;
}
