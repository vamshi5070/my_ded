{
  inputs = { nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable"; };
  outputs = { self, nixpkgs }: {
    defaultPackage.aarch64-linux =
      with import nixpkgs { system = "aarch64-linux"; };
      stdenv.mkDerivation {
        name = "hello";
        src = self;
        # buildPhase = "ls";
        buildInputs = [ pkgs.gcc
        # pkgs.ghc      
                        pkgs.SDL2 pkgs.SDL2_ttf pkgs.freetype
                        pkgs.glew
                        # pkgs.glfw
                        pkgs.stb];
        nativeBuildInputs =
          [ pkgs.pkg-config pkgs.SDL2_ttf pkgs.SDL2
            pkgs.glfw
          # pkgs.ghc
          pkgs.which pkgs.freetype pkgs.glew pkgs.stb];
          shellHook = ''
            export GHC_PKG_PATH=$(dirname $(dirname $(which ghc)))/lib/ghc-$(ghc --numeric-version)
          '';
          buildPhase = ''
               gcc -Wall -Wextra -std=c11  `pkg-config --cflags sdl2 SDL2_ttf freetype2 glew` -o hello ./tsoding/main.c ./tsoding/la.c ./tsoding/editor.c -lSDL2  -lSDL2_ttf  `pkg-config --libs sdl2 stb freetype2 glew` -lm
          '';
          installPhase = ''
            mkdir -p $out/bin
            install -t $out/bin hello
          '';
      };
  };

}
              # NIX_CFLAGS_COMPILE="$(pkg-config --cflags sdl2 SDL2_ttf glfw3 freetype2) $NIX_CFLAGS_COMPILE"
            # g++ -Wall -Wextra   `pkg-config --cflags sdl2 SDL2_ttf freetype2 glfw3 glew` -o hello ./cherno/Application.cpp -lSDL2  -lSDL2_ttf  `pkg-config --libs sdl2 stb freetype2 glew glfw3` -lGL -lm




# gcc -Wall -Wextra -std=c11  `pkg-config --cflags sdl2 SDL2_ttf freetype2 glew` -o hello ./src/main.c -lSDL2  -lSDL2_ttf  `pkg-config --libs sdl2 stb freetype2 glew` -lm


#     buildPhase = ''
#     NIX_CFLAGS_COMPILE="$(pkg-config --cflags sdl2 SDL2_ttf ) $NIX_CFLAGS_COMPILE"
#     ghc -dynamic -shared -fPIC -o libRetrieveText.so ./sdl2-c-hs/RetrieveText.hs
#     gcc -Wall -Wextra -std=c11  -o hello ./sdl2-c-hs/main.c -lSDL2  -lSDL2_ttf -I "$(dirname $(dirname $(which ghc)))/lib/ghc-$(ghc --numeric-version)"/rts/include -L$"$(dirname $(dirname $(which ghc)))/lib/ghc-$(ghc --numeric-version)"/rts/ -lHSrts-ghc$(ghc --numeric-version) -L. libRetrieveText.so
#   '';
#   installPhase = ''
#     mkdir -p $out/bin
#     mkdir $out/lib
#     cat libRetrieveText.so > $out/lib/libRetrieveText.so
#     export LD_LIBRARY_PATH=$out/lib/:$LD_LIBRARY_PATH
#     install -t $out/bin hello
#   '';
# };

# echo $LD_LIBRARY_PATH > $out/bin/hello
# echo $LD_LIBRARY_PATH > $out
# export LD_LIBRARY_PATH=$out/bin:$LD_LIBRARY_PATH
# echo $LD_LIBRARY_PATH $out/bin/vamshi

# export LD_LIBRARY_PATH=$(readlink -f libFFI.so)

# echo $LD_LIBRARY_PATH > $out
# $(readlink -f libRetrieveText.so)

# cat $(echo $LD_LIBRARY_PATH) > $out  
# export LD_LIBRARY_PATH=libRetrieveText.so
# cp libRetrieveText.so $out/bin/
# mkdir -p $out/bin; install -t $out/bin hello
# 
# echo $(realpath -s libRetrieveText.so) > $out
# mkdir -p $out/bin; install -t $out/bin hello; install -t $out/bin libRetrieveText.so
# -L. -lRetrieveText 

# -ldl  -L. -lRetrieveText 

# ghc --make ./sdl2-c-hs/RetrieveText.hs
# ghc -dynamic -shared -fPIC -o libRetrieveText.so ./sdl2-c-hs/RetrieveText.hs
# gcc -Wall -Wextra -std=c11  -o hello ./src/main.c -lSDL2 -lSDL2_ttf 

/* simple running command:
gcc -Wall -Wextra -std=c11  -o hello ./src/main.c -lSDL2 -lSDL2_ttf
*/

# gcc -Wall -Wextra -std=c11 -I "$(dirname $(dirname $(which ghc)))/lib/ghc-$(ghc --numeric-version)"/rts/include/  -o hello ./sdl2-c-hs/main.c -lSDL2  -ldl  -L. -lRetrieveText 

# gcc -Wall -Wextra -std=c11 -I "$(dirname $(dirname $(which ghc)))/lib/ghc-$(ghc --numeric-version)"/rts/include/ -pedantic -ggdb `pkg-config --cflags sdl2 SDL2_ttf ` -o hello ./sdl2-c-hs/main.c `pkg-config --libs sdl2 SDL2_ttf` -lm -ldl  -L "$(dirname $(dirname $(which ghc)))/lib/ghc-$(ghc --numeric-version)"/ 

# -lm -L. -lRetrieveText 
# ghc -o RetrieveText.o -c ./sdl2-c-hs/RetrieveText.hs
# rts/include/HsFFI.h

# touch $out/HsFFI.h
# $out/HsFFI.h
# install -D -m 644
# GHC_PKG_PATH="$(dirname $(dirname $(which ghc)))/lib/ghc-$(ghc --numeric-version)  $GHC_PKG_PATH"
# `$(${pkgs.ghc} --print-libdir)`/include"   -L$GHC_PKG_PATH/rts
# $GHC_PKG_PATH/include
# gcc -Wall -Wextra -std=c11 -pedantic -ggdb `pkg-config --cflags sdl2 sdl2_ttf ` -o hello ./pikuma.c `pkg-config --libs sdl2 sdl2_ttf` -lm

/* touch HsFFI.h
cat "$(dirname $(dirname $(which ghc)))/lib/ghc-$(ghc --numeric-version)"/rts/include/HsFFI.h > HsFFI.h
*/
