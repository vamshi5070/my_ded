#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
/* #include <SDL2/SDL.h> */

#define FONT_WIDTH 128
#define FONT_HEIGHT 64


void scc(int code){
  if (code < 0) {
    fprintf(stderr, "SDL ERROR: %s\n", SDL_GetError());
    exit(1);
  }
}

void *scp(void *ptr){
  if (ptr == NULL) {
    fprintf(stderr, "SDL ERROR: %s\n", SDL_GetError());
    exit(1);
  }

  return ptr;
}

int main(int argc, char* argv[]) {

  scc(SDL_Init(SDL_INIT_VIDEO));

  SDL_Window * window = scp(SDL_CreateWindow("Ted", 0,0,800,600,SDL_WINDOW_RESIZABLE));
  SDL_Renderer *renderer = scp(SDL_CreateRenderer(window,-1,SDL_RENDERER_ACCELERATED));

  /* int number = 10; */
  /* char numberText[10]; */
  /* snprintf(numberText, sizeof(numberText), "%d", number); */
  /* SDL_Color textColor = { 255, 255, 255 }; // Text color (white) */
  /* TTF_Font* gFont = TTF_OpenFont("./bitstream/VeraMono.ttf", 24); */

  /* SDL_Surface* textSurface = TTF_RenderText_Solid(gFont, numberText, textColor); */
  /* SDL_Texture* textTexture = SDL_CreateTextureFromSurface(renderer, textSurface); */
  /* SDL_Rect font_rect = { */
    /* .x = 0 */
    /* , .y = 0 */
    /* , .w = textSurface -> w */
    /* , .h = textSurface -> h */
  /* }; */
			
  /* SDL_Surface *surface = scp(SDL_CreateRGBSurfaceFrom(font */
						      /* ,   */
  /* SDL_Texture * texture = scp(SDL_CreateTexture(renderer */
						/* ,SDL_PIXELFORMAT_INDEX8 */
						/* ,SDL_TEXTUREACCESS_STATIC */
						/* ,FONT_WIDTH */
						/* ,FONT_HEIGHT)); */

  bool quit = false;
  while(!quit) {
    SDL_Event event = {0};
    while (SDL_PollEvent(&event)) {
      switch (event.type) {
      case SDL_QUIT: {
	quit = true;
      }
	break;
      }
    }
    scc(SDL_SetRenderDrawColor(renderer,0,0,0,255));
    scc(SDL_RenderClear(renderer));
    /* scc(SDL_RenderCopy(renderer,textTexture,&font_rect, &font_rect)); */
    /* SDL_DestroyTexture(textTexture); */
    /* SDL_FreeSurface(textSurface); */
    SDL_RenderPresent(renderer);
  }
  SDL_Quit();
  /* printf("Hello world"); */
  return 0;
}
