module Main where

import Foreign.C.Types
import Foreign.Ptr
import Foreign.Marshal.Array

foreign export ccall getIntList ::IO (Ptr CInt)

getIntList :: IO (Ptr CInt)
getIntList = do
  let intList = [1,2,3,4,5]
  newArray intList

main :: IO()
main = return ()
