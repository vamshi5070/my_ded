#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include "./my_globals.c"


// Function to render text on the screen
void renderText(const char* text, TTF_Font* font, int x, int y,SDL_Color color) {
  if (strlen(text) == 0) {
    return;
  }
  
  textSurface = TTF_RenderText_Solid(font, text,color);
  /* (SDL_Color){255, 255, 255, 255}); */
  textTexture = SDL_CreateTextureFromSurface(renderer, textSurface);
  
  SDL_Rect textRect;
  textRect.x = x;
  textRect.y = y;
  textRect.w = textSurface->w;
  textRect.h = textSurface->h;
  
  SDL_RenderCopy(renderer, textTexture, NULL, &textRect);
}

void drawSquareWithNumber(int x, int y, int number,int index) {
  SDL_Rect squareRect = { x, y, SQUARE_SIZE, SQUARE_SIZE };
  if (index != visiting_index) {
      SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255); // Square color (black)
  } else {
      SDL_SetRenderDrawColor(renderer, 255, 255, 0, 255); // Square color (black)
  } 

  SDL_RenderFillRect(renderer, &squareRect);

  SDL_Color textColor = { 0, 0, 0};
  SDL_Color indexColor = {255, 0, 255 }; // Text color (white)

  SDL_Color textFlipColor = { 0, 0, 0};
  SDL_Color indexFlipColor = {255, 0, 255 }; // Text color (white)

  char numberText[10],indexText[10];
  snprintf(numberText, sizeof(numberText), "%d", number);
  snprintf(indexText, sizeof(indexText), "%d", index);

  SDL_Surface* textSurface = TTF_RenderText_Solid(font_large, numberText, textColor);
  SDL_Texture* textTexture = SDL_CreateTextureFromSurface(renderer, textSurface);
  SDL_Rect textRect = { x + SQUARE_SIZE / 2 - textSurface->w / 2, y + SQUARE_SIZE / 2 - textSurface->h / 2, textSurface->w, textSurface->h };

  renderText(indexText,font_small,x,y+10,indexColor);
    
  SDL_RenderCopy(renderer, textTexture, NULL, &textRect);
  SDL_DestroyTexture(textTexture);
  SDL_FreeSurface(textSurface);
}

void drawMsgBuffer(SDL_Color _color){
  int x = 0;
  int y = WINDOW_HEIGHT * 7/8;
  SDL_Rect bufferRect = {x,y,WINDOW_WIDTH,WINDOW_HEIGHT};
  SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255); // Square color (black)
  SDL_RenderFillRect(renderer, &bufferRect);
}
