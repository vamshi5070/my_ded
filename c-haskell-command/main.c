#include <stdio.h>
#include "HsFFI.h"

extern char* modifyString(char*);

int main() {
  hs_init(NULL,NULL);
  char command[20];
  printf("Enter any command: ");
  
  if(!scanf("%s",command)){
    printf("Failed to get anything. ");
    return -1;
  }

  printf("Entered %s",command);
  char *modifiedString = modifyString(command); 
  printf("Upcased string %s",modifiedString);

  return 0;
  hs_exit();
}
