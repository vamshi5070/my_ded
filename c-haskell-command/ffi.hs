{-# LANGUAGE ForeignFunctionInterface #-}

import Foreign.C.Types
import Foreign.C.String
import Data.Char

foreign export ccall modifyString :: CString ->  IO CString

modifyString :: CString -> IO CString
modifyString str = do
  original <- peekCString str
  let modified = map toUpper original
  newCString modified
