{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    
  };
  outputs = {self,nixpkgs}: {
    defaultPackage.aarch64-linux =
      with import nixpkgs {system = "aarch64-linux";};
      stdenv.mkDerivation {
        name = "hello";
        src = self;
        # buildPhase = "ls";
        buildInputs = [pkgs.gcc pkgs.SDL2];
        nativeBuildInputs = [pkgs.pkg-config];
        buildPhase = ''
                   NIX_CFLAGS_COMPILE="$(pkg-config --cflags sdl2) $NIX_CFLAGS_COMPILE"
                   gcc -Wall -Wextra -std=c11 -pedantic -ggdb `pkg-config --cflags sdl2` -o hello ./hello.c `pkg-config --libs sdl2` -lm
        '';
        installPhase = "mkdir -p $out/bin; install -t $out/bin hello";
      };
  };
}
